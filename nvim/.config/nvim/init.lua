-- User settings --
    require('user/options')
    require('user/mappings')
    require('user/plugins')
    require('user/hooks')
    require('user/junks')

-- functionality
    vim.cmd('filetype indent plugin on')
