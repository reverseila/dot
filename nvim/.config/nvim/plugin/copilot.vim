" Fix problem with nvim-cmp
" source: https://github.com/NvChad/NvChad/issues/597#issuecomment-966035273
" let g:copilot_no_tab_map = v:true
" let g:copilot_assume_mapped = v:true
